# This definition stops the following lines choking
# if HOME isn't defined.
HOME                    = .
RANDFILE                = $ENV::HOME/.rnd

# Extra OBJECT IDENTIFIER info:
#oid_file               = $ENV::HOME/.oid
oid_section             = new_oids

# To use this configuration file with the "-extfile" option of the
# "openssl x509" utility, name here the section containing the
# X.509v3 extensions to use:
extensions              = server_cert
# (Alternatively, use a configuration file that has only
# X.509v3 extensions in its main [= default] section.)

[ new_oids ]

# We can add new OIDs in here for use by 'ca', 'req' and 'ts'.
# Add a simple OID like this:
# testoid1              = 1.2.3.4
# Or use config file substitution like this:
# testoid2              = ${testoid1}.5.6

# Policies used by the TSA examples.
tsa_policy1             = 1.2.3.4.1
tsa_policy2             = 1.2.3.4.5.6
tsa_policy3             = 1.2.3.4.5.7

####################################################################
[ ca ]
default_ca              = CA_default            # The default ca section

####################################################################
[ CA_default ]

dir                     = $ENV::KEY_DIR         # Where everything is kept
certs                   = $dir                  # Where the issued certs are kept
new_certs_dir           = $dir/newcerts         # default place for new certs.
#unique_subject         = no                    # Set to 'no' to allow creation of
                                                # several ctificates with same subject.
certificate             = $ENV::CACRT           # The CA certificate
database                = $ENV::DBINDEX         # database index file.
serial                  = $ENV::DBSERIAL        # The current serial number

crl_dir                 = $dir                  # Where the issued crl are kept
crl                     = $ENV::CRL             # The current CRL
crlnumber               = $ENV::CRLSERIAL       # the current crl number
                                                # must be commented out to leave a V1 CRL
private_key             = $ENV::CAKEY           # The private key
RANDFILE                = $dir/private/.rand    # private random number file

x509_extensions         = usr_cert              # The extentions to add to the cert

# Comment out the following two lines for the "traditional"
# (and highly broken) format.
name_opt                = ca_default            # Subject Name options
cert_opt                = ca_default            # Certificate field options

# Extension copying option: use with caution.
# copy_extensions = copy

crl_extensions          = crl_ext

default_days            = $ENV::CA_EXPIRE       # how long to certify for
default_crl_days        = 30                    # how long before next CRL
default_md              = default               # use public key default MD
preserve                = no                    # keep passed DN ordering

# A few difference way of specifying how similar the request should look
# For type CA, the listed attributes must be the same, and the optional
# and supplied fields are just that :-)
policy                  = policy_match

# For the CA policy
[ policy_match ]
countryName             = match
stateOrProvinceName     = match
localityName            = optional
organizationName        = match
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ policy_anything ]
countryName             = optional
stateOrProvinceName     = optional
localityName            = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

####################################################################
[ req ]
default_bits            = $ENV::KEY_SIZE
default_keyfile         = privkey.pem
distinguished_name      = req_distinguished_name
attributes              = req_attributes
# The extentions to add to the self signed cert
x509_extensions         = v3_ca
string_mask             = utf8only

# Passwords for private keys if not present they will be prompted for
# input_password        = secret
# output_password       = secret

# The extensions to add to a certificate request
# req_extensions        = v3_req

[ req_distinguished_name ]
countryName             = Country Name (2 letter code)
countryName_default     = $ENV::KEY_C
countryName_min         = 2
countryName_max         = 2

stateOrProvinceName     = State or Province Name (full name)
stateOrProvinceName_default     = $ENV::KEY_ST

localityName            = Locality Name (eg, city)
localityName_default    = $ENV::KEY_L

0.organizationName      = Organization Name (eg, company)
0.organizationName_default      = $ENV::KEY_ORG

organizationalUnitName  = Organizational Unit Name (eg, section)
organizationalUnitName_default = $ENV::KEY_OU

commonName              = Common Name (e.g. server FQDN or YOUR name)
commonName_default      = $ENV::KEY_CN
commonName_max          = 64

emailAddress            = Email Address
emailAddress_default    = $ENV::KEY_EMAIL
emailAddress_max        = 64

# SET-ex3               = SET extension number 3

[ req_attributes ]
challengePassword       = A challenge password
challengePassword_min   = 4
challengePassword_max   = 20

unstructuredName        = An optional company name

[ server_cert ]
# These extensions are added when 'ca' signs a request.
basicConstraints        = CA:FALSE
keyUsage                = digitalSignature, keyEncipherment
extendedKeyUsage        = serverAuth
subjectKeyIdentifier    = hash
authorityKeyIdentifier  = keyid,issuer:always

[ usr_cert ]
# These extensions are added when 'ca' signs a request.
basicConstraints        = CA:FALSE
keyUsage                = nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage        = clientAuth
subjectKeyIdentifier    = hash
authorityKeyIdentifier  = keyid,issuer

subjectAltName          = email:copy
issuerAltName           = issuer:copy

# This is required for TSA certificates.
# extendedKeyUsage      = critical,timeStamping

[ v3_req ]
# Extensions to add to a certificate request

basicConstraints        = CA:FALSE
keyUsage                = nonRepudiation, digitalSignature, keyEncipherment

[ v3_ca ]
# Extensions for a typical CA
# PKIX recommendation.

basicConstraints        = CA:true
keyUsage                = cRLSign, keyCertSign
subjectKeyIdentifier    = hash
authorityKeyIdentifier  = keyid:always,issuer

subjectAltName          = email:copy
issuerAltName           = issuer:copy

[ crl_ext ]
# CRL extensions.
# Only issuerAltName and authorityKeyIdentifier make any sense in a CRL.

# issuerAltName=issuer:copy
authorityKeyIdentifier  = keyid:always

[ proxy_cert_ext ]
# These extensions should be added when creating a proxy certificate

basicConstraints        = CA:FALSE

# This is typical in keyUsage for a client certificate.
# keyUsage = nonRepudiation, digitalSignature, keyEncipherment

# PKIX recommendations harmless if included in all certificates.
subjectKeyIdentifier    = hash
authorityKeyIdentifier  = keyid,issuer

# This stuff is for subjectAltName and issuerAltname.
# Import the email address.
# subjectAltName        = email:copy
# An alternative to produce certificates that aren't
# deprecated according to PKIX.
# subjectAltName        = email:move

# Copy subject details
# issuerAltName         = issuer:copy

# This really needs to be in place for it to be a proxy certificate.
proxyCertInfo           = critical,language:id-ppl-anyLanguage,pathlen:3,policy:foo

####################################################################
[ tsa ]
default_tsa             = tsa_config1           # the default TSA section

[ tsa_config1 ]
# These are used by the TSA reply generation only.
dir                     = $ENV::KEY_DIR         # TSA root directory
serial                  = $dir/tsaserial        # The current serial number (mandatory)
crypto_device           = builtin               # OpenSSL engine to use for signing
signer_cert             = $dir/tsacert.pem      # The TSA signing certificate
                                                # (optional)
certs                   = $dir/cacert.pem       # Certificate chain to include in reply
                                                # (optional)
signer_key              = $dir/private/tsakey.pem # The TSA private key (optional)

default_policy          = tsa_policy1           # Policy if request did not specify it
                                                # (optional)
other_policies          = tsa_policy2, tsa_policy3    # acceptable policies (optional)
digests                 = md5, sha1             # Acceptable message digests (mandatory)
accuracy                = secs:1, millisecs:500, microsecs:100    # (optional)
clock_precision_digits  = 0                     # number of digits after dot. (optional)
ordering                = yes                   # Is ordering defined for timestamps?
                                                # (optional, default: no)
tsa_name                = yes                   # Must the TSA name be included in the reply?
                                                # (optional, default: no)
ess_cert_id_chain       = no                    # Must the ESS cert id chain be included?
                                                # (optional, default: no)
